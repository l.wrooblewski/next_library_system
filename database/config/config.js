module.exports = {
  development: {
    dialect: 'sqlite',
    storage: process.env.SQLITE || '.tmp/data.db'
  },
  test: {
    dialect: 'sqlite',
    storage: process.env.SQLITE || '.tmp/data.db'
  },
  production: {
    dialect: 'sqlite',
    storage: process.env.SQLITE || '.tmp/data.db'
  }
}