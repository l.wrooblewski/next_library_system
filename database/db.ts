import { Sequelize, Options } from 'sequelize'
import configs from './config/config.js'

const env = process.env.NODE_ENV || 'development'
const config = (configs as unknown as {[key: string]: Options})[env]

const db: Sequelize = new Sequelize({
    ...config,
    define: {
        underscored: true
    }
})

export default db
