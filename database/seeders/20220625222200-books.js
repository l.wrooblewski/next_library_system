"use strict";

module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.bulkInsert(
      "Books",
      [
        {
          id: 1,
          title: "The Land Before Time VIII: The Big Freeze",
          isbn: "921693865-X",
          publish_year: 2011,
          authors_id: 1
        },
        {
          id: 2,
          title: "Naked Prey, The",
          isbn: "053220949-4",
          publish_year: 2004,
          authors_id: 76
        },
        {
          id: 3,
          title: "When a Stranger Calls",
          isbn: "097637998-8",
          publish_year: 1999,
          authors_id: 27
        },
        {
          id: 4,
          title: "Stevie",
          isbn: "296903985-0",
          publish_year: 1997,
          authors_id: 15
        },
        {
          id: 5,
          title: "Double Solitaire",
          isbn: "341581294-4",
          publish_year: 2001,
          authors_id: 31
        },
        {
          id: 6,
          title: "Attraction",
          isbn: "456818052-X",
          publish_year: 2000,
          authors_id: 44
        },
        {
          id: 7,
          title: "Freeheld",
          isbn: "836640392-0",
          publish_year: 1994,
          authors_id: 7
        },
        {
          id: 8,
          title: "Lay the Favorite",
          isbn: "407629022-4",
          publish_year: 2002,
          authors_id: 81
        },
        {
          id: 9,
          title: "In Your Eyes",
          isbn: "133845733-0",
          publish_year: 1993,
          authors_id: 4
        },
        {
          id: 10,
          title: "Nô",
          isbn: "525110996-2",
          publish_year: 2012,
          authors_id: 72
        },
        {
          id: 11,
          title:
            "Rumble in the Air-Conditioned Auditorium: O'Reilly vs. Stewart 2012, The",
          isbn: "849141043-0",
          publish_year: 2010,
          authors_id: 27
        },
        {
          id: 12,
          title: "Fat Head",
          isbn: "870927586-X",
          publish_year: 1986,
          authors_id: 99
        },
        {
          id: 13,
          title: "Gate of Hell (Jigokumon)",
          isbn: "329217813-4",
          publish_year: 1999,
          authors_id: 45
        },
        {
          id: 14,
          title: "Two Bits",
          isbn: "909760033-2",
          publish_year: 1994,
          authors_id: 8
        },
        {
          id: 15,
          title: "Black Swan",
          isbn: "127877217-0",
          publish_year: 2012,
          authors_id: 58
        },
        {
          id: 16,
          title: "Marriage of Maria Braun, The (Ehe der Maria Braun, Die)",
          isbn: "194546293-0",
          publish_year: 2012,
          authors_id: 21
        },
        {
          id: 17,
          title: "Mon Paradis - Der Winterpalast",
          isbn: "174798760-3",
          publish_year: 1985,
          authors_id: 66
        },
        {
          id: 18,
          title: "Hannie Caulder",
          isbn: "548470999-7",
          publish_year: 2005,
          authors_id: 75
        },
        {
          id: 19,
          title: "Cesar & Rosalie (César et Rosalie)",
          isbn: "383767015-5",
          publish_year: 2006,
          authors_id: 5
        },
        {
          id: 20,
          title: "Down by Law",
          isbn: "246138284-0",
          publish_year: 2006,
          authors_id: 82
        },
        {
          id: 21,
          title: "Soloist, The",
          isbn: "079230919-7",
          publish_year: 1992,
          authors_id: 11
        },
        {
          id: 22,
          title: "Committed",
          isbn: "678725321-4",
          publish_year: 2004,
          authors_id: 18
        },
        {
          id: 23,
          title: "Crimes of the Heart",
          isbn: "413583632-1",
          publish_year: 1998,
          authors_id: 37
        },
        {
          id: 24,
          title: "Dust to Glory",
          isbn: "725145470-X",
          publish_year: 1998,
          authors_id: 60
        },
        {
          id: 25,
          title: "Honest Man: The Life of R. Budd Dwyer",
          isbn: "931225907-5",
          publish_year: 2010,
          authors_id: 77
        },
        {
          id: 26,
          title: "Tokyo Drifter (Tôkyô nagaremono)",
          isbn: "720961188-6",
          publish_year: 1989,
          authors_id: 31
        },
        {
          id: 27,
          title: "I Shot a Man in Vegas",
          isbn: "680264438-X",
          publish_year: 1995,
          authors_id: 100
        },
        {
          id: 28,
          title: "First Power, The",
          isbn: "678558149-4",
          publish_year: 2012,
          authors_id: 39
        },
        {
          id: 29,
          title: "Dark Command",
          isbn: "111152627-3",
          publish_year: 2008,
          authors_id: 66
        },
        {
          id: 30,
          title: "Secret Things (Choses secrètes)",
          isbn: "040136185-3",
          publish_year: 2009,
          authors_id: 18
        },
        {
          id: 31,
          title: "Emergency Squad",
          isbn: "673842564-3",
          publish_year: 1985,
          authors_id: 52
        },
        {
          id: 32,
          title: "Patty Hearst",
          isbn: "742252659-9",
          publish_year: 1997,
          authors_id: 90
        },
        {
          id: 33,
          title: "Chronicles of Riddick, The",
          isbn: "142424331-9",
          publish_year: 2003,
          authors_id: 76
        },
        {
          id: 34,
          title: "Squall, The",
          isbn: "740681178-0",
          publish_year: 1990,
          authors_id: 7
        },
        {
          id: 35,
          title: "Great Day in Harlem, A",
          isbn: "120083540-9",
          publish_year: 1992,
          authors_id: 17
        },
        {
          id: 36,
          title: "Party Monster",
          isbn: "559140963-6",
          publish_year: 2000,
          authors_id: 70
        },
        {
          id: 37,
          title: "Starship Troopers 3: Marauder",
          isbn: "902175461-4",
          publish_year: 1984,
          authors_id: 15
        },
        {
          id: 38,
          title: "East Meets West (Dung sing sai tsau 2011)",
          isbn: "405504649-9",
          publish_year: 1985,
          authors_id: 43
        },
        {
          id: 39,
          title: "Hearts in Atlantis",
          isbn: "868110901-4",
          publish_year: 1994,
          authors_id: 15
        },
        {
          id: 40,
          title: "Late Show, The",
          isbn: "509429502-0",
          publish_year: 1997,
          authors_id: 4
        },
        {
          id: 41,
          title: "Best Worst Movie",
          isbn: "228996573-1",
          publish_year: 1987,
          authors_id: 92
        },
        {
          id: 42,
          title: "One Potato, Two Potato",
          isbn: "300580081-4",
          publish_year: 2011,
          authors_id: 21
        },
        {
          id: 43,
          title: "CrissCross",
          isbn: "744672620-2",
          publish_year: 2004,
          authors_id: 19
        },
        {
          id: 44,
          title: "Dead Man's Bluff",
          isbn: "540316269-0",
          publish_year: 2003,
          authors_id: 67
        },
        {
          id: 45,
          title: "Winter of Frozen Dreams",
          isbn: "833716010-1",
          publish_year: 2002,
          authors_id: 36
        },
        {
          id: 46,
          title: "Treasure Planet",
          isbn: "363923248-8",
          publish_year: 2001,
          authors_id: 82
        },
        {
          id: 47,
          title: "Diggstown",
          isbn: "898278170-6",
          publish_year: 2009,
          authors_id: 55
        },
        {
          id: 48,
          title: "Endurance",
          isbn: "176159072-3",
          publish_year: 2003,
          authors_id: 81
        },
        {
          id: 49,
          title: "Day and Night (Le jour et la nuit)",
          isbn: "082004284-6",
          publish_year: 1996,
          authors_id: 62
        },
        {
          id: 50,
          title: "Karen Cries on the Bus",
          isbn: "243858212-X",
          publish_year: 1999,
          authors_id: 86
        },
        {
          id: 51,
          title: "Born to Fight (Kerd ma lui)",
          isbn: "760677813-2",
          publish_year: 2010,
          authors_id: 97
        },
        {
          id: 52,
          title: "Wing Chun",
          isbn: "439275144-9",
          publish_year: 2011,
          authors_id: 49
        },
        {
          id: 53,
          title: "Growth",
          isbn: "490575058-X",
          publish_year: 2004,
          authors_id: 89
        },
        {
          id: 54,
          title: "Canvas",
          isbn: "969037022-7",
          publish_year: 1995,
          authors_id: 95
        },
        {
          id: 55,
          title: "I'm Still Here",
          isbn: "726924699-8",
          publish_year: 1995,
          authors_id: 20
        },
        {
          id: 56,
          title: "Stella",
          isbn: "942281033-7",
          publish_year: 2009,
          authors_id: 59
        },
        {
          id: 57,
          title: "Afflicted, The",
          isbn: "688933915-0",
          publish_year: 1992,
          authors_id: 97
        },
        {
          id: 58,
          title: "Frozen Days",
          isbn: "855073764-X",
          publish_year: 2007,
          authors_id: 75
        },
        {
          id: 59,
          title: "Brenda Starr",
          isbn: "695152323-1",
          publish_year: 1997,
          authors_id: 53
        },
        {
          id: 60,
          title: "Green Chair (Noksaek uija)",
          isbn: "015976644-3",
          publish_year: 1985,
          authors_id: 27
        },
        {
          id: 61,
          title: "Blaze",
          isbn: "833314812-3",
          publish_year: 1986,
          authors_id: 42
        },
        {
          id: 62,
          title: "Funny Games U.S.",
          isbn: "624933788-1",
          publish_year: 2007,
          authors_id: 60
        },
        {
          id: 63,
          title: "Bill Maher... But I'm Not Wrong",
          isbn: "033806144-4",
          publish_year: 2006,
          authors_id: 27
        },
        {
          id: 64,
          title: "Invisible Agent",
          isbn: "411889857-8",
          publish_year: 2009,
          authors_id: 26
        },
        {
          id: 65,
          title: "Event Horizon",
          isbn: "472253574-4",
          publish_year: 1996,
          authors_id: 31
        },
        {
          id: 66,
          title: "Kapò",
          isbn: "878671586-0",
          publish_year: 2006,
          authors_id: 54
        },
        {
          id: 67,
          title: "TV Junkie",
          isbn: "855448192-5",
          publish_year: 2006,
          authors_id: 35
        },
        {
          id: 68,
          title: "Out at the Wedding",
          isbn: "917704718-4",
          publish_year: 1999,
          authors_id: 83
        },
        {
          id: 69,
          title: "Skyscraper Souls",
          isbn: "928871066-3",
          publish_year: 1997,
          authors_id: 12
        },
        {
          id: 70,
          title: "Louis Cyr: The Strongest Man in the World",
          isbn: "189268267-2",
          publish_year: 1970,
          authors_id: 67
        },
        {
          id: 71,
          title: "8 (8, the Play)",
          isbn: "774657463-7",
          publish_year: 2005,
          authors_id: 94
        },
        {
          id: 72,
          title: "Caine (Shark!)",
          isbn: "804402009-8",
          publish_year: 2000,
          authors_id: 44
        },
        {
          id: 73,
          title: "12th & Delaware",
          isbn: "575176122-7",
          publish_year: 2006,
          authors_id: 53
        },
        {
          id: 74,
          title: "Peace, Propaganda & the Promised Land",
          isbn: "600101507-4",
          publish_year: 1995,
          authors_id: 36
        },
        {
          id: 75,
          title: "Scott Joplin",
          isbn: "183785317-7",
          publish_year: 2000,
          authors_id: 99
        },
        {
          id: 76,
          title: "Maps to the Stars",
          isbn: "324935619-0",
          publish_year: 2012,
          authors_id: 45
        },
        {
          id: 77,
          title: "She Gods of Shark Reef",
          isbn: "921910997-2",
          publish_year: 1997,
          authors_id: 99
        },
        {
          id: 78,
          title: "Gagarin: First in Space",
          isbn: "677701433-0",
          publish_year: 2005,
          authors_id: 80
        },
        {
          id: 79,
          title: "Frozen",
          isbn: "805707579-1",
          publish_year: 2006,
          authors_id: 54
        },
        {
          id: 80,
          title: "Monga",
          isbn: "597133111-6",
          publish_year: 2000,
          authors_id: 8
        },
        {
          id: 81,
          title: "Antonia's Line (Antonia)",
          isbn: "954323237-7",
          publish_year: 2009,
          authors_id: 78
        },
        {
          id: 82,
          title: "Life and Death of Peter Sellers, The",
          isbn: "889989888-X",
          publish_year: 1993,
          authors_id: 90
        },
        {
          id: 83,
          title: "My Lucky Stars (Fuk sing go jiu)",
          isbn: "570608347-9",
          publish_year: 2003,
          authors_id: 14
        },
        {
          id: 84,
          title: "State Property",
          isbn: "608471872-8",
          publish_year: 2002,
          authors_id: 77
        },
        {
          id: 85,
          title: "Beck - Familjen",
          isbn: "392026113-5",
          publish_year: 2008,
          authors_id: 41
        },
        {
          id: 86,
          title: "Fan, The",
          isbn: "827984626-3",
          publish_year: 1982,
          authors_id: 37
        },
        {
          id: 87,
          title: "Poor Us: An Animated History of Poverty",
          isbn: "275551462-0",
          publish_year: 2003,
          authors_id: 58
        },
        {
          id: 88,
          title: "Hero",
          isbn: "316767234-X",
          publish_year: 1996,
          authors_id: 72
        },
        {
          id: 89,
          title: "Leave",
          isbn: "105957766-6",
          publish_year: 2009,
          authors_id: 11
        },
        {
          id: 90,
          title: "One Small Hitch",
          isbn: "080089077-9",
          publish_year: 2002,
          authors_id: 17
        },
        {
          id: 91,
          title: "Fabulous Baker Boys, The",
          isbn: "867853046-4",
          publish_year: 2008,
          authors_id: 44
        },
        {
          id: 92,
          title: "Leave Her to Heaven",
          isbn: "196113564-7",
          publish_year: 2000,
          authors_id: 86
        },
        {
          id: 93,
          title: "Erik the Viking",
          isbn: "163516964-X",
          publish_year: 2012,
          authors_id: 63
        },
        {
          id: 94,
          title: "Fourth Angel, The",
          isbn: "778638664-5",
          publish_year: 1970,
          authors_id: 43
        },
        {
          id: 95,
          title:
            "Master of the Flying Guillotine (Du bi quan wang da po xue di zi)",
          isbn: "666724733-0",
          publish_year: 2004,
          authors_id: 14
        },
        {
          id: 96,
          title: "Evil Under the Sun",
          isbn: "310851310-9",
          publish_year: 1998,
          authors_id: 33
        },
        {
          id: 97,
          title: "Tabu: A Story of the South Seas",
          isbn: "680565260-X",
          publish_year: 2004,
          authors_id: 36
        },
        {
          id: 98,
          title: "Once Bitten",
          isbn: "621721125-4",
          publish_year: 2004,
          authors_id: 94
        },
        {
          id: 99,
          title: "Talk of the Town, The",
          isbn: "680552122-X",
          publish_year: 2004,
          authors_id: 98
        },
        {
          id: 100,
          title: "Shock",
          isbn: "835079930-7",
          publish_year: 1997,
          authors_id: 38
        }
      ],
      {}
    );
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete("Books", null, {});
  }
};
