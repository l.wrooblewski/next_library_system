"use strict";

module.exports = {
  async up(queryInterface) {
    return queryInterface.bulkInsert("Roles", [
      {
        id: 1,
        role_name: "reader"
      },
      {
        id: 2,
        role_name: "admin"
      }
    ]);
  },

  async down(queryInterface) {
    await queryInterface.bulkDelete("Roles", null, {});
  }
};
