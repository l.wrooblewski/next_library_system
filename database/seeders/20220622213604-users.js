"use strict";

module.exports = {
  async up(queryInterface) {
    return queryInterface.bulkInsert("Users", [
      {
        id: 1,
        name: "Jude",
        surname: "Guthrum",
        email: "jguthrum0@ow.ly",
        birthday: "1973-10-18",
        gender: 1,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 2,
        name: "Leonardo",
        surname: "Haining",
        email: "lhaining1@diigo.com",
        birthday: "1979-07-28",
        gender: 1,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 3,
        name: "Shalom",
        surname: "Wardale",
        email: "swardale2@cbslocal.com",
        birthday: "1987-11-08",
        gender: 0,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 4,
        name: "Leontine",
        surname: "Carragher",
        email: "lcarragher3@meetup.com",
        birthday: "2001-03-15",
        gender: 0,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 5,
        name: "Clarette",
        surname: "Almack",
        email: "calmack4@geocities.com",
        birthday: "1985-04-04",
        gender: 1,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 6,
        name: "Hastings",
        surname: "McGennis",
        email: "hmcgennis5@google.com",
        birthday: "1988-05-01",
        gender: 1,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 7,
        name: "Yetta",
        surname: "Fitzpayn",
        email: "yfitzpayn6@w3.org",
        birthday: "1981-07-29",
        gender: 1,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 8,
        name: "Randie",
        surname: "Lace",
        email: "rlace7@drupal.org",
        birthday: "1999-12-30",
        gender: 0,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 9,
        name: "Stanfield",
        surname: "Franek",
        email: "sfranek8@biglobe.ne.jp",
        birthday: "1992-11-05",
        gender: 1,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 10,
        name: "Jocko",
        surname: "Oldham",
        email: "joldham9@wikimedia.org",
        birthday: "1992-05-28",
        gender: 1,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 11,
        name: "Avrom",
        surname: "Egre",
        email: "aegrea@digg.com",
        birthday: "2002-12-17",
        gender: 1,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 12,
        name: "Ollie",
        surname: "Chatelot",
        email: "ochatelotb@apache.org",
        birthday: "1991-04-03",
        gender: 0,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 13,
        name: "Cody",
        surname: "Polotti",
        email: "cpolottic@drupal.org",
        birthday: "1959-06-28",
        gender: 0,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 14,
        name: "Gonzalo",
        surname: "Motion",
        email: "gmotiond@sciencedirect.com",
        birthday: "1971-01-21",
        gender: 0,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 15,
        name: "Josefa",
        surname: "Metzing",
        email: "jmetzinge@istockphoto.com",
        birthday: "1983-09-05",
        gender: 1,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 16,
        name: "Demetris",
        surname: "Goom",
        email: "dgoomf@wikimedia.org",
        birthday: "1967-07-10",
        gender: 1,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 17,
        name: "Marcella",
        surname: "Capel",
        email: "mcapelg@baidu.com",
        birthday: "1962-07-15",
        gender: 1,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 18,
        name: "Odetta",
        surname: "Nye",
        email: "onyeh@cornell.edu",
        birthday: "1974-09-13",
        gender: 1,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 19,
        name: "Tremayne",
        surname: "Kilvington",
        email: "tkilvingtoni@cbsnews.com",
        birthday: "1986-04-13",
        gender: 1,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 20,
        name: "Vivien",
        surname: "Lightman",
        email: "vlightmanj@w3.org",
        birthday: "1976-12-17",
        gender: 1,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 21,
        name: "Gerladina",
        surname: "Gerler",
        email: "ggerlerk@yandex.ru",
        birthday: "1968-08-16",
        gender: 0,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 22,
        name: "Isis",
        surname: "MacGuiness",
        email: "imacguinessl@deviantart.com",
        birthday: "1982-01-30",
        gender: 1,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 23,
        name: "Steve",
        surname: "Bothen",
        email: "sbothenm@amazonaws.com",
        birthday: "1974-03-12",
        gender: 1,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 24,
        name: "Kendre",
        surname: "Di Nisco",
        email: "kdiniscon@zdnet.com",
        birthday: "1981-11-06",
        gender: 0,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 25,
        name: "Miquela",
        surname: "Tomaszkiewicz",
        email: "mtomaszkiewiczo@mayoclinic.com",
        birthday: "1977-05-06",
        gender: 1,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 26,
        name: "Rafferty",
        surname: "Phidgin",
        email: "rphidginp@ebay.co.uk",
        birthday: "1965-02-27",
        gender: 0,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 27,
        name: "Daria",
        surname: "Bastone",
        email: "dbastoneq@smugmug.com",
        birthday: "1957-07-26",
        gender: 1,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 28,
        name: "Ferne",
        surname: "Clohessy",
        email: "fclohessyr@wix.com",
        birthday: "1982-06-21",
        gender: 0,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 29,
        name: "Kasey",
        surname: "Locke",
        email: "klockes@networksolutions.com",
        birthday: "1985-10-13",
        gender: 1,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 30,
        name: "Cletus",
        surname: "Kos",
        email: "ckost@nyu.edu",
        birthday: "1989-12-22",
        gender: 0,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 31,
        name: "Stavro",
        surname: "Edmons",
        email: "sedmonsu@hibu.com",
        birthday: "1971-10-11",
        gender: 1,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 32,
        name: "Vivian",
        surname: "Trewin",
        email: "vtrewinv@hubpages.com",
        birthday: "1982-06-30",
        gender: 1,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 33,
        name: "Verina",
        surname: "Burgisi",
        email: "vburgisiw@scientificamerican.com",
        birthday: "1954-12-25",
        gender: 1,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 34,
        name: "Lamond",
        surname: "Dangl",
        email: "ldanglx@home.pl",
        birthday: "1974-12-26",
        gender: 0,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 35,
        name: "Bibi",
        surname: "Schrei",
        email: "bschreiy@ycombinator.com",
        birthday: "1961-09-26",
        gender: 0,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 36,
        name: "Heddi",
        surname: "Garment",
        email: "hgarmentz@blogs.com",
        birthday: "1981-05-13",
        gender: 1,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 37,
        name: "Leisha",
        surname: "Wellbank",
        email: "lwellbank10@linkedin.com",
        birthday: "1969-03-31",
        gender: 0,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 38,
        name: "Nike",
        surname: "Tuffell",
        email: "ntuffell11@163.com",
        birthday: "1983-03-29",
        gender: 0,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 39,
        name: "Edik",
        surname: "Davie",
        email: "edavie12@psu.edu",
        birthday: "1951-01-30",
        gender: 0,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 40,
        name: "Land",
        surname: "Girt",
        email: "lgirt13@tinypic.com",
        birthday: "1987-04-17",
        gender: 0,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 41,
        name: "Garik",
        surname: "Flatman",
        email: "gflatman14@marketwatch.com",
        birthday: "1953-10-18",
        gender: 0,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 42,
        name: "Fern",
        surname: "Ellington",
        email: "fellington15@ovh.net",
        birthday: "1992-08-11",
        gender: 1,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 43,
        name: "Werner",
        surname: "Hugk",
        email: "whugk16@i2i.jp",
        birthday: "1955-12-20",
        gender: 1,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 44,
        name: "Essie",
        surname: "Stafford",
        email: "estafford17@ning.com",
        birthday: "1953-12-06",
        gender: 0,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 45,
        name: "Quinton",
        surname: "Ayres",
        email: "qayres18@thetimes.co.uk",
        birthday: "1962-05-02",
        gender: 0,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 46,
        name: "Cayla",
        surname: "Gaggen",
        email: "cgaggen19@imgur.com",
        birthday: "1961-04-11",
        gender: 0,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 47,
        name: "Levin",
        surname: "Gwinnett",
        email: "lgwinnett1a@fema.gov",
        birthday: "1977-04-24",
        gender: 0,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 48,
        name: "Cass",
        surname: "Cluelow",
        email: "ccluelow1b@instagram.com",
        birthday: "1983-03-26",
        gender: 1,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 49,
        name: "Rodney",
        surname: "Colomb",
        email: "rcolomb1c@networksolutions.com",
        birthday: "1971-11-25",
        gender: 0,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 50,
        name: "Rafaello",
        surname: "Carnell",
        email: "rcarnell1d@delicious.com",
        birthday: "2000-09-25",
        gender: 0,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 51,
        name: "Hernando",
        surname: "Mersh",
        email: "hmersh1e@npr.org",
        birthday: "1985-01-06",
        gender: 1,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 52,
        name: "Towny",
        surname: "Sayles",
        email: "tsayles1f@alexa.com",
        birthday: "1999-11-22",
        gender: 0,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 53,
        name: "Binky",
        surname: "Hearmon",
        email: "bhearmon1g@phpbb.com",
        birthday: "1956-04-29",
        gender: 0,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 54,
        name: "Nissa",
        surname: "McCaffery",
        email: "nmccaffery1h@wikispaces.com",
        birthday: "1993-01-01",
        gender: 0,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 55,
        name: "Valdemar",
        surname: "Avramow",
        email: "vavramow1i@unicef.org",
        birthday: "2003-10-04",
        gender: 0,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 56,
        name: "Vite",
        surname: "Thirtle",
        email: "vthirtle1j@state.tx.us",
        birthday: "1975-10-23",
        gender: 1,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 57,
        name: "Jordan",
        surname: "Lalor",
        email: "jlalor1k@wisc.edu",
        birthday: "1988-07-23",
        gender: 0,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 58,
        name: "Margette",
        surname: "Grain",
        email: "mgrain1l@hao123.com",
        birthday: "1953-08-20",
        gender: 1,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 59,
        name: "Rosanna",
        surname: "Andrat",
        email: "randrat1m@miitbeian.gov.cn",
        birthday: "2000-04-13",
        gender: 1,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 60,
        name: "Holden",
        surname: "Frunks",
        email: "hfrunks1n@cnbc.com",
        birthday: "1968-02-06",
        gender: 0,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 61,
        name: "Thea",
        surname: "Izatson",
        email: "tizatson1o@independent.co.uk",
        birthday: "1979-09-21",
        gender: 1,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 62,
        name: "Betteann",
        surname: "Harses",
        email: "bharses1p@gravatar.com",
        birthday: "1980-05-13",
        gender: 0,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 63,
        name: "Marys",
        surname: "Whitmarsh",
        email: "mwhitmarsh1q@etsy.com",
        birthday: "2000-03-27",
        gender: 0,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 64,
        name: "Amitie",
        surname: "Gibbeson",
        email: "agibbeson1r@unc.edu",
        birthday: "1978-01-22",
        gender: 1,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 65,
        name: "Thomas",
        surname: "Busswell",
        email: "tbusswell1s@shutterfly.com",
        birthday: "1958-09-28",
        gender: 1,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 66,
        name: "Nettie",
        surname: "Jaycock",
        email: "njaycock1t@studiopress.com",
        birthday: "1982-12-12",
        gender: 1,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 67,
        name: "Bill",
        surname: "Tearney",
        email: "btearney1u@prlog.org",
        birthday: "1974-05-20",
        gender: 0,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 68,
        name: "Merci",
        surname: "de Courcy",
        email: "mdecourcy1v@mail.ru",
        birthday: "1999-02-23",
        gender: 0,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 69,
        name: "Benjie",
        surname: "Dufaire",
        email: "bdufaire1w@apache.org",
        birthday: "1971-03-28",
        gender: 0,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 70,
        name: "Janelle",
        surname: "Wiggin",
        email: "jwiggin1x@netscape.com",
        birthday: "1989-10-09",
        gender: 0,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 71,
        name: "Ali",
        surname: "Goundrill",
        email: "agoundrill1y@taobao.com",
        birthday: "2000-03-17",
        gender: 0,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 72,
        name: "Iolande",
        surname: "Lamborne",
        email: "ilamborne1z@squarespace.com",
        birthday: "1981-08-13",
        gender: 1,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 73,
        name: "Brittne",
        surname: "Vasyukov",
        email: "bvasyukov20@reddit.com",
        birthday: "1952-10-13",
        gender: 0,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 74,
        name: "Auguste",
        surname: "Varlow",
        email: "avarlow21@bandcamp.com",
        birthday: "1952-01-24",
        gender: 1,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 75,
        name: "Walden",
        surname: "Bicheno",
        email: "wbicheno22@ft.com",
        birthday: "1988-01-06",
        gender: 0,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 76,
        name: "Haleigh",
        surname: "Vaines",
        email: "hvaines23@etsy.com",
        birthday: "1999-07-28",
        gender: 0,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 77,
        name: "Katusha",
        surname: "Elener",
        email: "kelener24@bigcartel.com",
        birthday: "1998-06-06",
        gender: 1,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 78,
        name: "Hayward",
        surname: "Bowsher",
        email: "hbowsher25@addthis.com",
        birthday: "1960-05-24",
        gender: 0,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 79,
        name: "Porty",
        surname: "Wattingham",
        email: "pwattingham26@icq.com",
        birthday: "1983-07-05",
        gender: 1,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 80,
        name: "Gun",
        surname: "Russilll",
        email: "grussilll27@hhs.gov",
        birthday: "1981-02-02",
        gender: 0,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 81,
        name: "Werner",
        surname: "Feast",
        email: "wfeast28@mac.com",
        birthday: "1989-07-10",
        gender: 1,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 82,
        name: "Stavros",
        surname: "Fieldhouse",
        email: "sfieldhouse29@moonfruit.com",
        birthday: "1979-06-25",
        gender: 0,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 83,
        name: "Filia",
        surname: "Duckitt",
        email: "fduckitt2a@imdb.com",
        birthday: "1980-11-03",
        gender: 0,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 84,
        name: "Lennard",
        surname: "Carradice",
        email: "lcarradice2b@cdbaby.com",
        birthday: "1996-05-23",
        gender: 0,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 85,
        name: "Gregoire",
        surname: "Kach",
        email: "gkach2c@oaic.gov.au",
        birthday: "1957-03-05",
        gender: 0,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 86,
        name: "Jilli",
        surname: "Canty",
        email: "jcanty2d@linkedin.com",
        birthday: "1958-11-01",
        gender: 1,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 87,
        name: "Bren",
        surname: "Summerside",
        email: "bsummerside2e@wikia.com",
        birthday: "1983-03-14",
        gender: 1,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 88,
        name: "Javier",
        surname: "Fallis",
        email: "jfallis2f@slashdot.org",
        birthday: "1955-07-01",
        gender: 0,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 89,
        name: "Elenore",
        surname: "Balle",
        email: "eballe2g@amazon.de",
        birthday: "1987-12-04",
        gender: 1,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 90,
        name: "Cyrill",
        surname: "Purselowe",
        email: "cpurselowe2h@phpbb.com",
        birthday: "1977-01-01",
        gender: 0,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 91,
        name: "Jacquelynn",
        surname: "Cartin",
        email: "jcartin2i@twitter.com",
        birthday: "1964-10-10",
        gender: 0,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 92,
        name: "Ham",
        surname: "Blackney",
        email: "hblackney2j@people.com.cn",
        birthday: "2003-07-11",
        gender: 1,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 93,
        name: "Eadith",
        surname: "Sheran",
        email: "esheran2k@prnewswire.com",
        birthday: "1974-04-17",
        gender: 0,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 94,
        name: "Carson",
        surname: "Labrone",
        email: "clabrone2l@example.com",
        birthday: "1992-10-28",
        gender: 0,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 95,
        name: "Miltie",
        surname: "Vanyashkin",
        email: "mvanyashkin2m@fotki.com",
        birthday: "1962-12-27",
        gender: 1,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 96,
        name: "Talya",
        surname: "Dreher",
        email: "tdreher2n@omniture.com",
        birthday: "1979-07-17",
        gender: 1,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 97,
        name: "Danyelle",
        surname: "Varne",
        email: "dvarne2o@xinhuanet.com",
        birthday: "1961-06-29",
        gender: 1,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 98,
        name: "Odille",
        surname: "Yurenev",
        email: "oyurenev2p@soundcloud.com",
        birthday: "1952-02-29",
        gender: 1,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 99,
        name: "Lev",
        surname: "Bugdale",
        email: "lbugdale2q@stumbleupon.com",
        birthday: "1954-07-21",
        gender: 0,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 100,
        name: "Bat",
        surname: "Wycliff",
        email: "bwycliff2r@alexa.com",
        birthday: "1969-03-05",
        gender: 0,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 1
      },
      {
        id: 101,
        name: "Admin",
        surname: "Bibliotekarz",
        email: "example@example.com",
        birthday: "1969-03-05",
        gender: 0,
        password: "dddd5d7b474d2c78ebbb833789c4bfd721edf4bf",
        roles_id: 2
      }
    ]);
  },

  async down(queryInterface) {
    return queryInterface.bulkDelete("Users", null, {});
  }
};
