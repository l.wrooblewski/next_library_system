const DataTypes = require("sequelize").DataTypes;

module.exports = {
  up: async (queryInterface, Sequelize) => {
    // await queryInterface.addConstraint("users", {
    //   fields: ["roles_id"],
    //   type: "foreign key",
    //   name: "users_roles_id_fkey",
    //   references: {
    //     table: "roles",
    //     field: "id"
    //   }
    // });

    await queryInterface.addConstraint("books", {
      fields: ["authors_id"],
      type: "foreign key",
      name: "books_authors_id_fkey",
      references: {
        table: "authors",
        field: "id"
      }
    });

    await queryInterface.addConstraint("rentals", {
      fields: ["users_id"],
      type: "foreign key",
      name: "rentals_users_id_fkey",
      references: {
        table: "users",
        field: "id"
      }
    });

    await queryInterface.addConstraint("rentals", {
      fields: ["books_id"],
      type: "foreign key",
      name: "rentals_books_id_fkey",
      references: {
        table: "books",
        field: "id"
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeConstraint("users", "users_roles_id_fkey");
    await queryInterface.removeConstraint("books", "books_authors_id_fkey");
    await queryInterface.removeConstraint("rentals", "rentals_users_id_fkey");
    await queryInterface.removeConstraint("rentals", "rentals_books_id_fkey");
  }
};
