const DataTypes = require("sequelize").DataTypes;

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable("users", {
      id: {
        type: DataTypes.INTEGER,
        field: "id",
        primaryKey: true,
        autoIncrement: true,
        allowNull: false,
        unique: true
      },
      name: {
        type: DataTypes.STRING,
        field: "name",
        allowNull: false
      },
      surname: {
        type: DataTypes.STRING,
        field: "surname",
        allowNull: false
      },
      birthday: {
        type: DataTypes.DATEONLY,
        field: "birthday"
      },
      gender: {
        type: DataTypes.INTEGER,
        field: "gender"
      },
      password: {
        type: DataTypes.STRING,
        field: "password",
        allowNull: false
      },
      email: {
        type: DataTypes.STRING,
        field: "email",
        allowNull: false
      },
      createdAt: {
        type: DataTypes.DATE,
        field: "created_at"
      },
      updatedAt: {
        type: DataTypes.DATE,
        field: "updated_at"
      },
      rolesId: {
        type: DataTypes.INTEGER,
        field: "roles_id"
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("users");
  }
};
