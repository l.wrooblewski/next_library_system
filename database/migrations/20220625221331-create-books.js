const DataTypes = require("sequelize").DataTypes;

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable("books", {
      id: {
        type: DataTypes.INTEGER,
        field: "id",
        primaryKey: true,
        autoIncrement: true,
        allowNull: false,
        unique: true
      },
      title: {
        type: DataTypes.STRING,
        field: "title",
        allowNull: false
      },
      isbn: {
        type: DataTypes.STRING,
        field: "isbn",
        allowNull: false
      },
      publishYear: {
        type: DataTypes.INTEGER,
        field: "publish_year",
        allowNull: false
      },
      createdAt: {
        type: DataTypes.DATE,
        field: "created_at"
      },
      updatedAt: {
        type: DataTypes.DATE,
        field: "updated_at"
      },
      authorsId: {
        type: DataTypes.STRING,
        field: "authors_id"
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("books");
  }
};
