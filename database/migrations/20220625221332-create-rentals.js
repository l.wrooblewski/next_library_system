const DataTypes = require('sequelize').DataTypes

module.exports = {
    up: async (queryInterface, Sequelize) => {
        await queryInterface.createTable('rentals', {
            id: {
                type: DataTypes.INTEGER,
                field: 'id',
                primaryKey: true,
                autoIncrement: true,
                allowNull: false,
                unique: true
            },
            dueDate: {
                type: DataTypes.DATEONLY,
                field: 'due_date'
            },
            returnedDate: {
                type: DataTypes.DATEONLY,
                field: 'returned_date'
            },
            createdAt: {
                type: DataTypes.DATE,
                field: 'created_at'
            },
            updatedAt: {
                type: DataTypes.DATE,
                field: 'updated_at'
            },
            usersId: {
                type: DataTypes.INTEGER,
                field: 'users_id'
            },
            booksId: {
                type: DataTypes.INTEGER,
                field: 'books_id'
            }
        })
    },
    down: async (queryInterface, Sequelize) => {
        await queryInterface.dropTable('rentals');
    },
};