import {
    Association,
    BelongsToGetAssociationMixin,
    BelongsToSetAssociationMixin,
    BelongsToCreateAssociationMixin,
    CreationOptional,
    DataTypes,
    InferCreationAttributes,
    InferAttributes,
    Model,
    NonAttribute,
    Sequelize
} from 'sequelize'
import type { Book } from './Book'
import type { User } from './User'

type RentalAssociations = 'user' | 'book'

export class Rental extends Model<
    InferAttributes<Rental, {omit: RentalAssociations}>,
    InferCreationAttributes<Rental, {omit: RentalAssociations}>
    > {
    declare id: CreationOptional<number>
    declare dueDate: string | null
    declare returnedDate: string | null
    declare createdAt: CreationOptional<Date>
    declare updatedAt: CreationOptional<Date>

    // Rental belongsTo User
    declare user?: NonAttribute<User>
    declare getUser: BelongsToGetAssociationMixin<User>
    declare setUser: BelongsToSetAssociationMixin<User, number>
    declare createUser: BelongsToCreateAssociationMixin<User>

    // Rental belongsTo Book
    declare book?: NonAttribute<Book>
    declare getBook: BelongsToGetAssociationMixin<Book>
    declare setBook: BelongsToSetAssociationMixin<Book, number>
    declare createBook: BelongsToCreateAssociationMixin<Book>

    declare static associations: {
        user: Association<Rental, User>,
        book: Association<Rental, Book>
    }

    static initModel(sequelize: Sequelize): typeof Rental {
        Rental.init({
            id: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                autoIncrement: true,
                allowNull: false,
                unique: true
            },
            dueDate: {
                type: DataTypes.DATEONLY
            },
            returnedDate: {
                type: DataTypes.DATEONLY
            },
            createdAt: {
                type: DataTypes.DATE
            },
            updatedAt: {
                type: DataTypes.DATE
            }
        }, {
            sequelize
        })

        return Rental
    }
}