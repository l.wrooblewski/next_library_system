import type { Sequelize, Model } from 'sequelize'
import { User } from './User'
import { Role } from './Role'
import { Author } from './Author'
import { Book } from './Book'
import { Rental } from './Rental'

export {
    User,
    Role,
    Author,
    Book,
    Rental
}

export function initModels(sequelize: Sequelize) {
    User.initModel(sequelize)
    Role.initModel(sequelize)
    Author.initModel(sequelize)
    Book.initModel(sequelize)
    Rental.initModel(sequelize)

    User.belongsTo(Role, {
        as: 'role',
        foreignKey: 'roles_id'
    })
    Book.belongsTo(Author, {
        as: 'author',
        foreignKey: 'authors_id'
    })
    Rental.belongsTo(User, {
        as: 'user',
        foreignKey: 'users_id'
    })
    Rental.belongsTo(Book, {
        as: 'book',
        foreignKey: 'books_id'
    })

    return {
        User,
        Role,
        Author,
        Book,
        Rental
    }
}