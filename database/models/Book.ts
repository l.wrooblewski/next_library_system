import {
    Association,
    BelongsToGetAssociationMixin,
    BelongsToSetAssociationMixin,
    BelongsToCreateAssociationMixin,
    CreationOptional,
    DataTypes,
    InferCreationAttributes,
    InferAttributes,
    Model,
    NonAttribute,
    Sequelize
} from 'sequelize'
import type { Author } from './Author'

type BookAssociations = 'author'

export class Book extends Model<
    InferAttributes<Book, {omit: BookAssociations}>,
    InferCreationAttributes<Book, {omit: BookAssociations}>
    > {
    declare id: CreationOptional<number>
    declare title: string
    declare isbn: string
    declare publishYear: number
    declare createdAt: CreationOptional<Date>
    declare updatedAt: CreationOptional<Date>

    // Book belongsTo Author
    declare author?: NonAttribute<Author>
    declare getAuthor: BelongsToGetAssociationMixin<Author>
    declare setAuthor: BelongsToSetAssociationMixin<Author, number>
    declare createAuthor: BelongsToCreateAssociationMixin<Author>

    declare static associations: {
        author: Association<Book, Author>
    }

    static initModel(sequelize: Sequelize): typeof Book {
        Book.init({
            id: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                autoIncrement: true,
                allowNull: false,
                unique: true
            },
            title: {
                type: DataTypes.STRING,
                allowNull: false
            },
            isbn: {
                type: DataTypes.STRING,
                allowNull: false
            },
            publishYear: {
                type: DataTypes.INTEGER,
                allowNull: false
            },
            createdAt: {
                type: DataTypes.DATE
            },
            updatedAt: {
                type: DataTypes.DATE
            }
        }, {
            sequelize
        })

        return Book
    }
}