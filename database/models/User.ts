import {
  Association,
  BelongsToGetAssociationMixin,
  BelongsToSetAssociationMixin,
  BelongsToCreateAssociationMixin,
  CreationOptional,
  DataTypes,
  InferCreationAttributes,
  InferAttributes,
  Model,
  NonAttribute,
  Sequelize
} from 'sequelize'
import type { Role } from './Role'

type UserAssociations = 'role'

export class User extends Model<
    InferAttributes<User, {omit: UserAssociations}>,
    InferCreationAttributes<User, {omit: UserAssociations}>
    > {
  declare id: CreationOptional<number>
  declare name: string
  declare surname: string
  declare birthday: string | null
  declare gender: number | null
  declare password: string
  declare email: string
  declare rolesId: number
  declare createdAt: CreationOptional<Date>
  declare updatedAt: CreationOptional<Date>

  // User belongsTo Role
  declare role?: NonAttribute<Role>
  declare getRole: BelongsToGetAssociationMixin<Role>
  declare setRole: BelongsToSetAssociationMixin<Role, number>
  declare createRole: BelongsToCreateAssociationMixin<Role>

  declare static associations: {
    role: Association<User, Role>
  }

  static initModel(sequelize: Sequelize): typeof User {
    User.init({
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false,
        unique: true
      },
      name: {
        type: DataTypes.STRING,
        allowNull: false
      },
      surname: {
        type: DataTypes.STRING,
        allowNull: false
      },
      birthday: {
        type: DataTypes.DATEONLY
      },
      gender: {
        type: DataTypes.INTEGER
      },
      password: {
        type: DataTypes.STRING,
        allowNull: false
      },
      email: {
        type: DataTypes.STRING,
        allowNull: false
      },
      rolesId: {
        type: DataTypes.INTEGER
      },
      createdAt: {
        type: DataTypes.DATE
      },
      updatedAt: {
        type: DataTypes.DATE
      }
    }, {
      sequelize
    })

    return User
  }
}