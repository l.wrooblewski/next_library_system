import { ReactNode, useEffect, useState } from "react";
import navigationJson from "../../config/navigation.json";
import { useUserData } from "../../contexts/UserContext/useUserData";
import Cookies from "js-cookie";
import { useRouter } from "next/router";
import { TNavigation } from "../../config/Navigation";

const navigation = (navigationJson as unknown) as Record<string, string>[];

interface Props {
  title: string;
  children: ReactNode;
  navigation?: TNavigation;
}
export const DashboardLayout = ({ title, children }: Props) => {
  const [navigationItems, setNavigationItems] = useState([]);
  // @ts-ignore
  const { userData, setUserData } = useUserData() || { userData: {} };
  const router = useRouter();

  useEffect(() => {
    if (Object.keys(userData).length) {
      // @ts-ignore
      setNavigationItems(navigation[userData?.role?.roleName]);
    }
  }, [userData]);

  return (
    <>
      <nav
        className="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0"
        style={{ padding: "10px !important" }}
      >
        <a className="navbar-brand col-sm-3 col-md-2 mr-0" href="#">
          next_library_system
        </a>
        <ul className="navbar-nav px-3">
          <li className="nav-item text-nowrap">
            <a
              className="nav-link"
              href="#"
              onClick={() => {
                Cookies.remove("authToken");
                Cookies.remove("userId");
                setUserData({});
                router.push("/");
              }}
            >
              Wyloguj
            </a>
          </li>
        </ul>
      </nav>

      <div className="container-fluid">
        <div className="row">
          <nav className="col-md-2 d-none d-md-block bg-light sidebar">
            <div className="sidebar-sticky">
              <ul className="nav flex-column">
                {navigationItems.map(({ label, url }) => (
                  <li key={`${label}__${url}`} className="nav-item">
                    <a className="nav-link" href={url}>
                      {label}
                    </a>
                  </li>
                ))}

                {/*<li className="nav-item">*/}
                {/*  <a className="nav-link active" href="#">*/}
                {/*    Wypożyczenia*/}
                {/*  </a>*/}
                {/*</li>*/}
                {/*<li className="nav-item">*/}
                {/*  <a className="nav-link" href="#">*/}
                {/*    Upomnienia*/}
                {/*  </a>*/}
                {/*</li>*/}
                {/*<li className="nav-item">*/}
                {/*  <a className="nav-link" href="#">*/}
                {/*    Kary*/}
                {/*  </a>*/}
                {/*</li>*/}
              </ul>
            </div>
          </nav>

          <main role="main" className="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
            <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
              <h1 className="h2">{title}</h1>
            </div>
            {children}
          </main>
        </div>
      </div>
    </>
  );
};
