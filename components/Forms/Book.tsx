import * as Yup from "yup";
import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { IAuthor, getAllAuthors } from "../../lib/authors";
import { Field, Form, Formik } from "formik";
import { addBook, editBook } from "../../lib/books";
import swal from "sweetalert2";
import { MDBAlert, MDBBtn, MDBRow } from "mdbreact";

interface BookForm {
  authors_id?: number | any;
  title: string;
  isbn: string;
  publishYear: number;
  authorId: number;
}

const BookSchema = Yup.object().shape({
  title: Yup.string()
    .min(2, "Too Short!")
    .max(150, "Too Long!")
    .required("Required"),
  isbn: Yup.string()
    .min(2, "Too Short!")
    .max(10, "Too Long!")
    .required("Required"),
  publishYear: Yup.number()
    .min(1900, "Too old!")
    .max(2022, "Too young!")
    .required("Required")
});

interface Props {
  id?: string;
  defaultValues?: BookForm;
}

export const BookForm = ({ id, defaultValues }: Props) => {
  const [authors, setAuthors] = useState([]);

  const router = useRouter();
  useEffect(() => {
    if (!authors.length) {
      getAllAuthors().then((response: any) => setAuthors(response.data));
    }
  }, [authors]);
  return (
    <MDBRow start>
      <Formik
        validationSchema={BookSchema}
        initialValues={
          defaultValues
            ? { ...defaultValues, authorId: defaultValues.authors_id }
            : {
                title: "",
                isbn: "",
                publishYear: 2022,
                authorId: 1
              }
        }
        enableReinitialize
        onSubmit={(values: BookForm) => {
          console.log({ values });
          if (!defaultValues) {
            return addBook(values).then(response => {
              console.log({ response });
              if (response.status === "success") {
                swal.fire("Wprowadzono zmiany!", "", "success");
                router.push("/admin/books");
              }
            });
          } else {
            // @ts-ignore
            return editBook(defaultValues.id, values).then(response => {
              console.log({ response });
              if (response.status === "success") {
                swal.fire("Wprowadzono zmiany!", "", "success");
                router.push("/admin/books");
              }
            });
          }
        }}
      >
        {({ errors, touched }) => (
          <Form>
            <MDBRow start className="mb-3">
              <label htmlFor="title">Tytuł</label>
              <Field
                id="title"
                name="title"
                placeholder="Pan Tadeusz"
                type="text"
              />
              {errors.title && touched.title ? (
                <MDBAlert color="danger" className="mt-2">
                  {errors.title}
                </MDBAlert>
              ) : null}
            </MDBRow>
            <MDBRow start className="mb-3">
              <label htmlFor="ISBN">ISBN</label>
              <Field
                id="ISBN"
                name="isbn"
                placeholder="213-456-789-0"
                type="text"
              />
              {errors.isbn && touched.isbn ? (
                <MDBAlert color="danger" className="mt-2">
                  {errors.isbn}
                </MDBAlert>
              ) : null}
            </MDBRow>
            <MDBRow start className="mb-3">
              <label htmlFor="publishYear">Rok publikacji</label>
              <Field
                id="publishYear"
                name="publishYear"
                placeholder="2022"
                type="number"
                min={1900}
                max={2022}
              />
              {errors.publishYear && touched.publishYear ? (
                <MDBAlert color="danger" className="mt-2">
                  {errors.publishYear}
                </MDBAlert>
              ) : null}
            </MDBRow>
            <MDBRow start className="mb-3">
              <label htmlFor="authorId">Autor</label>
              <Field
                id="authorId"
                name="authorId"
                component="select"
                type="select"
              >
                {authors.map((author: IAuthor) => (
                  <option
                    key={author.id}
                    value={author.id}
                  >{`${author.name} ${author.surname}`}</option>
                ))}
              </Field>
              {errors.authorId && touched.authorId ? (
                <MDBAlert color="danger" className="mt-2">
                  {errors.authorId}
                </MDBAlert>
              ) : null}
            </MDBRow>

            <MDBBtn className="col-3 mb-3" color="primary" type="submit">
              Zapisz zmiany
            </MDBBtn>
          </Form>
        )}
      </Formik>
    </MDBRow>
  );
};
