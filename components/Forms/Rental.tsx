import * as Yup from "yup";
import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { Field, Form, Formik } from "formik";
import { getAllBooks } from "../../lib/books";
import swal from "sweetalert2";
import { MDBAlert, MDBBtn, MDBRow } from "mdbreact";
import { getAllReaders } from "../../lib/getUserData";
import { addRental, editRental, IRental } from "../../lib/rentals";

interface RentalForm {
  users_id?: number | any;
  books_id?: number | any;
  returnedDate: string;
  dueDate: string;
  userId?: any;
  bookId?: any;
}

const RentalSchema = Yup.object().shape({
  dueDate: Yup.date().required("Required")
});

interface Props {
  id?: string;
  defaultValues?: RentalForm;
}

export const RentalForm = ({ id, defaultValues }: Props) => {
  const [readers, setReaders] = useState([]);
  const [books, setBooks] = useState([]);

  const router = useRouter();
  useEffect(() => {
    if (!readers.length) {
      getAllReaders().then((response: any) => setReaders(response.data));
    }
    if (!books.length) {
      getAllBooks().then((response: any) => setBooks(response.data));
    }
  }, [books.length, readers]);
  return (
    <MDBRow start>
      <Formik
        validationSchema={RentalSchema}
        // @ts-ignore
        initialValues={
          defaultValues
            ? {
                ...defaultValues,
                bookId: Number(defaultValues.books_id),
                readerId: Number(defaultValues.users_id)
              }
            : {
                dueDate: "",
                returnedDate: "",
                bookId: 1,
                readerId: 1
              }
        }
        enableReinitialize
        onSubmit={(values: RentalForm) => {
          if (!defaultValues) {
            return addRental(values as IRental).then(response => {
              if (response.status === "success") {
                swal.fire("Wprowadzono zmiany!", "", "success");
                router.push("/admin/rentals");
              }
            });
          } else {
            // @ts-ignore
            return editRental(defaultValues.id, values).then(response => {
              if (response.status === "success") {
                swal.fire("Wprowadzono zmiany!", "", "success");
                router.push("/admin/rentals");
              }
            });
          }
        }}
      >
        {({ errors, touched }) => (
          <Form>
            <MDBRow start className="mb-3">
              <label htmlFor="title">Czytelnik</label>
              <Field
                id="readerId"
                name="readerId"
                component="select"
                type="select"
              >
                {readers.map((reader: any) => (
                  <option
                    key={reader.id}
                    value={reader.id}
                  >{`${reader.name} ${reader.surname}`}</option>
                ))}
              </Field>
            </MDBRow>
            <MDBRow start className="mb-3">
              <label htmlFor="bookId">Książka</label>
              <Field id="bookId" name="bookId" component="select" type="select">
                {books.map((book: any) => (
                  <option
                    key={book.id}
                    value={book.id}
                  >{`${book.title}`}</option>
                ))}
              </Field>
            </MDBRow>
            <MDBRow start className="mb-3">
              <label htmlFor="returnedDate">Data zwrócenia</label>
              <Field id="returnedDate" name="returnedDate" type="date" />
              {errors.returnedDate && touched.returnedDate ? (
                <MDBAlert color="danger" className="mt-2">
                  {errors.returnedDate}
                </MDBAlert>
              ) : null}
            </MDBRow>
            <MDBRow start className="mb-3">
              <label htmlFor="dueDate">Data do zwrotu</label>
              <Field id="dueDate" name="dueDate" type="date" />
              {errors.dueDate && touched.dueDate ? (
                <MDBAlert color="danger" className="mt-2">
                  {errors.dueDate}
                </MDBAlert>
              ) : null}
            </MDBRow>

            <MDBBtn className="col-3 mb-3" color="primary" type="submit">
              Zapisz zmiany
            </MDBBtn>
          </Form>
        )}
      </Formik>
    </MDBRow>
  );
};
