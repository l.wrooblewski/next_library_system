import * as Yup from "yup";
import { useRouter } from "next/router";
import {
  addAuthor,
  IAuthor,
  getAllAuthors,
  editAuthor
} from "../../lib/authors";
import { Field, Form, Formik } from "formik";
import { editBook } from "../../lib/books";
import swal from "sweetalert2";
import { MDBAlert, MDBBtn, MDBRow } from "mdbreact";

interface AuthorForm {
  name: string;
  surname: string;
}

const AuthorSchema = Yup.object().shape({
  name: Yup.string()
    .min(2, "Too Short!")
    .max(50, "Too Long!")
    .required("Required"),
  surname: Yup.string()
    .min(2, "Too Short!")
    .max(50, "Too Long!")
    .required("Required")
});

interface Props {
  id?: string;
  defaultValues?: AuthorForm;
}

export const AuthorForm = ({ id, defaultValues }: Props) => {
  const router = useRouter();
  return (
    <MDBRow start>
      <Formik
        validationSchema={AuthorSchema}
        initialValues={
          defaultValues
            ? defaultValues
            : {
                name: "",
                surname: ""
              }
        }
        enableReinitialize
        onSubmit={(values: AuthorForm) => {
          if (!defaultValues) {
            return addAuthor(values as IAuthor).then(response => {
              console.log({ response });
              if (response.status === "success") {
                swal.fire("Wprowadzono zmiany!", "", "success");
                router.push("/admin/authors");
              }
            });
          } else {
            // @ts-ignore
            return editAuthor(defaultValues.id, values).then(response => {
              console.log({ response });
              if (response.status === "success") {
                swal.fire("Wprowadzono zmiany!", "", "success");
                router.push("/admin/authors");
              }
            });
          }
        }}
      >
        {({ errors, touched }) => (
          <Form>
            <MDBRow start className="mb-3">
              <label htmlFor="title">Imię</label>
              <Field id="name" name="name" placeholder="Jan" type="text" />
              {errors.name && touched.name ? (
                <MDBAlert color="danger" className="mt-2">
                  {errors.name}
                </MDBAlert>
              ) : null}
            </MDBRow>
            <MDBRow start className="mb-3">
              <label htmlFor="surname">Nazwisko</label>
              <Field
                id="surname"
                name="surname"
                placeholder="Kowalski"
                type="text"
              />
              {errors.surname && touched.surname ? (
                <MDBAlert color="danger" className="mt-2">
                  {errors.surname}
                </MDBAlert>
              ) : null}
            </MDBRow>

            <MDBBtn className="col-3 mb-3" color="primary" type="submit">
              Zapisz zmiany
            </MDBBtn>
          </Form>
        )}
      </Formik>
    </MDBRow>
  );
};
