import type { NextPage } from 'next'
import {DashboardLayout} from "../../components/DashboardLayout/DashboardLayout";
import {MDBAlert, MDBContainer, MDBDataTable, MDBRow} from "mdbreact";
import { MDBBtn } from "mdbreact";
import {useEffect, useState} from "react";
import { Book} from "../../database/models";
import {getAllBooks} from "../../lib/books";
import {NextPageContext} from "next";
import {SSRAuthRedirect} from "../../lib/SSRAuthRedirect";


const adaptData = (books: Book[]) => ({
    columns: [
        {
            label: 'id',
            field: 'id',
            sort: 'asc',
            width: 150
        },
        {
            label: 'Tytuł',
            field: 'title',
            sort: 'asc',
            width: 270
        },
        {
            label: 'ISBN',
            field: 'isbn',
            sort: 'asc',
            width: 200
        },
        {
            label: 'Rok publikacji',
            field: 'publishYear',
            sort: 'asc',
            width: 200
        },
        {
            label: 'Autor',
            field: 'authorName',
            sort: 'asc',
            width: 200
        },
    ],
    rows: books.map(({id, title, isbn, publishYear, author }) =>
        ({
            id,
            title,
            isbn,
            publishYear,
            authorName: `${author?.name} ${author?.surname}`,
        })),
});

const Books: NextPage = () => {
    const [books, setBooks] = useState([]);

    useEffect(() => {
        if (books.length === 0) {
            getAllBooks().then((response:any) => {
                setBooks(response.data)
            });
        }
    }, [books.length]);


    return (
        <DashboardLayout title="Katalog książek">
            <MDBContainer>
                <MDBDataTable
                    striped
                    bordered
                    hover
                    infoLabel={['Wyświetlono','do','z','wpisów']}
                    paginationLabel={['Wstecz', 'Dalej']}
                    searchLabel="Wyszukaj"
                    data={adaptData(books)}
                    displayEntries={false}
                /><MDBRow start>
            </MDBRow></MDBContainer></DashboardLayout>
    )
}

export default Books;

export async function getServerSideProps(context: NextPageContext) {
    return SSRAuthRedirect(context);
}
