import type { NextPage } from 'next'
import {DashboardLayout} from "../../components/DashboardLayout/DashboardLayout";
import {MDBContainer, MDBDataTable, MDBRow} from "mdbreact";
import {useEffect, useState} from "react";
import {getAllReadersRentals, IRental} from "../../lib/rentals";
import {NextRouter, useRouter} from "next/router";
import {getRentalStatus} from "../../lib/helpers";
import {useUserData} from "../../contexts/UserContext/useUserData";
import {NextPageContext} from "next";
import {SSRAuthRedirect} from "../../lib/SSRAuthRedirect";


const adaptData = (rentals: IRental[], router: NextRouter) => ({
    columns: [
        {
            label: 'id',
            field: 'id',
            sort: 'asc',
            width: 150
        },
        {
            label: 'Książka',
            field: 'bookField',
            sort: 'asc',
            width: 200
        },
        {
            label: 'Status',
            field: 'status',
            sort: 'asc',
            width: 200
        },
    ],
    rows: rentals.map(({id, book,dueDate,returnedDate,user }) => {
        return ({
            id,
            bookField: typeof(book) !== 'number' ? book?.title : book,
            status: getRentalStatus(dueDate, returnedDate),
        })
    }),
});

const Index: NextPage = () => {
    const [rentals, setRentals] = useState([]);
    const router = useRouter();
    // @ts-ignore
    const {userData} = useUserData();

    useEffect(() => {
        if (rentals.length === 0) {
            getAllReadersRentals(userData.id).then((response:any) => {
                setRentals(response.data)
            });
        }
    }, [rentals.length, userData.id]);


    return (
        <DashboardLayout title="Wypożyczenia">
            <MDBContainer>
                <MDBDataTable
                    striped
                    bordered
                    hover
                    infoLabel={['Wyświetlono','do','z','wpisów']}
                    paginationLabel={['Wstecz', 'Dalej']}
                    searchLabel="Wyszukaj"
                    data={adaptData(rentals, router)}
                    displayEntries={false}
                /><MDBRow start>
            </MDBRow></MDBContainer></DashboardLayout>
    )
}

export default Index;


export async function getServerSideProps(context: NextPageContext) {
    return SSRAuthRedirect(context);
}
