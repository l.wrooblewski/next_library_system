import nextConnect from "next-connect";
import { initModels } from "../../../database/models";
import db from "../../../database/db";

// const handler = nextConnect().get(async (req, res) => {
//   const models = await initModels(db);
//
//   const authors = await models.Author.findAll();
//
//   res.statusCode = 200;
//
//   res.json({
//     status: "success",
//     data: [...authors]
//   });
// });

const handler = nextConnect()
  .get(async (req, res) => {
    const models = await initModels(db);

    const { id } = req.query;
    let result;
    if (!id) {
      result = await models.Author.findAll();
    } else {
      result = await models.Author.findOne({
        where: {
          id
        }
      });
    }

    res.statusCode = 200;

    res.json({
      status: "success",
      data: result
    });
  })
  .put(async (req, res) => {
    const models = await initModels(db);
    const { surname, name } = req.body;
    console.log(1, req.body);
    const status = await models.Author.create({
      name,
      surname
    });

    console.log(status);

    res.statusCode = 200;

    res.json({
      status: "success",
      data: status
    });
  })
  .post(async (req, res) => {
    const models = await initModels(db);
    const { name, surname, id } = req.body;
    console.log({ name, surname, id });
    const status = await models.Author.update(
      {
        name,
        surname
      },
      {
        where: {
          id: Number(id)
        }
      }
    );

    res.statusCode = 200;

    res.json({
      status: "success",
      data: status
    });
  });

export default handler;
