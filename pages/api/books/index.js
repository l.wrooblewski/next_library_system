import nextConnect from "next-connect";
import { initModels } from "../../../database/models";
import db from "../../../database/db";

const handler = nextConnect()
  .get(async (req, res) => {
    const models = await initModels(db);

    const { id } = req.query;
    let result;
    if (!id) {
      result = await models.Book.findAll({
        include: [
          {
            model: models.Author,
            as: "author"
          }
        ]
      });
    } else {
      result = await models.Book.findOne({
        where: {
          id
        }
      });
    }

    res.statusCode = 200;

    res.json({
      status: "success",
      data: result
    });
  })
  .put(async (req, res) => {
    const models = await initModels(db);
    const { title, isbn, publishYear, authorId } = req.body;
    console.log(req.body);
    const status = await models.Book.create({
      title,
      isbn,
      publishYear,
      authors_id: authorId
    });

    res.statusCode = 200;

    res.json({
      status: "success",
      data: status
    });
  })
  .post(async (req, res) => {
    const models = await initModels(db);
    const { title, isbn, publishYear, authorId, id } = req.body;
    const status = await models.Book.update(
      {
        title,
        isbn,
        publishYear,
        authors_id: authorId
      },
      {
        where: {
          id
        }
      }
    );

    res.statusCode = 200;

    res.json({
      status: "success",
      data: status
    });
  });

export default handler;
