import nextConnect from "next-connect";
import { initModels } from "../../../database/models";
import db from "../../../database/db";

const handler = nextConnect()
  .get(async (req, res) => {
    const models = await initModels(db);

    const { id, userId } = req.query;
    let result;
    if (id && !userId) {
      result = await models.Rental.findOne({
        where: {
          id
        }
      });
    }
    if (!id && userId) {
      result = await models.Rental.findAll({
        where: {
          users_id: userId
        },
        include: [
          {
            model: models.User,
            as: "user"
          },
          {
            model: models.Book,
            as: "book"
          }
        ]
      });
    }

    if (!id && !userId) {
      result = await models.Rental.findAll({
        include: [
          {
            model: models.User,
            as: "user"
          },
          {
            model: models.Book,
            as: "book"
          }
        ]
      });
    }

    res.statusCode = 200;

    res.json({
      status: "success",
      data: result
    });
  })
  .put(async (req, res) => {
    const models = await initModels(db);
    const { dueDate, returnedDate, user, book } = req.body;

    const status = await models.Rental.create({
      dueDate: dueDate && dueDate !== "Invalid date" ? dueDate : null,
      returnedDate:
        returnedDate && returnedDate !== "Invalid date" ? returnedDate : null,
      users_id: Number(user),
      books_id: Number(book)
    });

    res.statusCode = 200;

    res.json({
      status: "success",
      data: status
    });
  })
  .post(async (req, res) => {
    const models = await initModels(db);
    const { dueDate, returnedDate, user, book, id } = req.body;
    const status = await models.Rental.update(
      {
        dueDate: dueDate && dueDate !== "Invalid date" ? dueDate : null,
        returnedDate:
          returnedDate && returnedDate !== "Invalid date" ? returnedDate : null,
        users_id: Number(user),
        books_id: Number(book)
      },
      {
        where: {
          id
        }
      }
    );

    res.statusCode = 200;

    res.json({
      status: "success",
      data: status
    });
  });

export default handler;
