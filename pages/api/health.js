import nextConnect from 'next-connect';
import { initModels } from '../../database/models/index';
import db from '../../database/db';

const handler = nextConnect().get(async (req, res) => {
    const models = await initModels(db);

    console.log(models);

    const users = await models.User.findAndCountAll({
        order: [
            ['id', 'DESC'],
        ],
    });
    res.statusCode = 200;
    res.json({
        status: 'success',
        data: users.rows,
        total: users.count,
    });
})

export default handler;