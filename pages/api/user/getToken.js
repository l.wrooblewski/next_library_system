import nextConnect from "next-connect";
import { initModels } from "../../../database/models";
import db from "../../../database/db";
import sha1 from "sha1";

const handler = nextConnect().get(async (req, res) => {
  const models = await initModels(db);
  const { email, password } = req.query;

  const user = await models.User.findOne({
    where: {
      email
    }
  });

  const role = await models.Role.findOne({
    where: {
      id: user.roles_id
    }
  });

  if (!user) {
    res.json({
      status: "error",
      error: "not found"
    });
  }

  const passwordHash = sha1(password);

  if (user.password === passwordHash) {
    res.statusCode = 200;

    res.json({
      status: "success",
      data: {
        token: sha1(`${user.id}`),
        user: {
          role,
          userId: user.id
        }
      }
    });
  }
});

export default handler;
