import nextConnect from "next-connect";
import { initModels } from "../../../database/models";
import db from "../../../database/db";
import sha1 from "sha1";

const handler = nextConnect().get(async (req, res) => {
  const models = await initModels(db);

  const readers = await models.User.findAll({
    where: {
      roles_id: 1
    }
  });

  if (!readers) {
    res.json({
      status: "error",
      error: "not found"
    });
  }

  res.statusCode = 200;

  res.json({
    status: "success",
    data: readers
  });
});

export default handler;
