import nextConnect from "next-connect";
import { initModels } from "../../../database/models";
import db from "../../../database/db";
import sha1 from "sha1";

const handler = nextConnect().get(async (req, res) => {
  const models = await initModels(db);
  const { token } = req.query;

  const users = await models.User.findAll({
    include: [
      {
        model: models.Role,
        as: "role"
      }
    ]
  });
  const user = users.find(user => sha1(`${user.getDataValue("id")}`) === token);

  if (!user) {
    res.json({
      status: "error",
      error: "not found"
    });
  }

  res.statusCode = 200;

  res.json({
    status: "success",
    data: user
  });
});

export default handler;
