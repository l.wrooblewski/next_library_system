import '../styles/globals.css'
import type { AppProps } from 'next/app'
import 'mdb-ui-kit/css/mdb.min.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import {UserContextProvider} from "../contexts/UserContext/UserContext";

// add usercontext here
function MyApp({ Component, pageProps }: AppProps) {
 return <UserContextProvider><Component {...pageProps} /></UserContextProvider>
}

export default MyApp
