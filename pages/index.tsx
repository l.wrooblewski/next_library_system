import type {NextPage, NextPageContext} from 'next'
import Image from 'next/image'
import {useState} from "react";
import {loginUser} from "../lib/loginUser";
import Cookies from 'js-cookie';
import {useRouter} from "next/router";
import {SSRAuthRedirect} from "../lib/SSRAuthRedirect";
import cookie from "cookie";

const Index: NextPage = () => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const router = useRouter();
    return (
        <div className="container-sm" style={{
            maxWidth: 360,
            marginTop: '20%'
        }}>
            <div style={{
                display: 'flex',
                justifyContent: 'center',
                marginBottom: 40,
            }}>
            <Image src="/logo.png" alt="Logo" height={64} width={256} style={{
                margin: '0 auto'
            }}/>
            </div>
        <form>
            <div className="form-outline mb-4">
                <input type="email" value={email} id="form2Example1" className="form-control" onChange={(e) => setEmail(e.target.value)}/>
                <label className="form-label" htmlFor="form2Example1">Adres email</label>
            </div>

            <div className="form-outline mb-4">
                <input type="password" value={password} id="form2Example2" className="form-control"  onChange={(e) => setPassword(e.target.value)}/>
                <label className="form-label" htmlFor="form2Example2">Hasło</label>
            </div>

            <button type="button" className="btn btn-primary btn-block mb-4" style={{
                marginTop: 20,
            }} onClick={() => {
                loginUser({email, password}).then((response: any) => {
                    if (response.status === "success") {
                        const { token, user } = response.data;
                        Cookies.set('authToken', token);
                        Cookies.set('userId', user.userId);
                        const role = user.role.roleName;
                        Cookies.set('role', role);
                        router.push(`/${role}/books`);
                    }
                })
                // redirect to app
            }}>Zaloguj</button>
        </form>
        </div>
    )
}

export default Index

export async function getServerSideProps(context: NextPageContext) {
    const cookies = cookie.parse(context?.req?.headers.cookie || "");
    if (
        cookies.userId &&
        cookies.authToken &&
        cookies.role
    ) {
        context.res?.writeHead(307, { Location: `/${cookies.role}/books` });
        context.res?.end();
    }
    return {
        props: {}
    };
}
