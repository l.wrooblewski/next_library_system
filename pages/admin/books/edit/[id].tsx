import type { NextPage } from 'next'
import {DashboardLayout} from "../../../../components/DashboardLayout/DashboardLayout";
import { MDBContainer} from "mdbreact";
import {BookForm} from "../../../../components/Forms/Book";
import {useRouter} from "next/router";
import {useEffect, useState} from "react";
import {getBookById} from "../../../../lib/books";
import {NextPageContext} from "next";
import {SSRAuthRedirect} from "../../../../lib/SSRAuthRedirect";

const EditBook: NextPage = () => {
    const router = useRouter();
    const { id } = router.query;
    const [defaultValues, setDefaultValues] = useState();

    useEffect(()=> {
        if (id && !defaultValues){
            getBookById(Number(id)).then(response => {
                setDefaultValues(response.data);
            })
        }
    }, [defaultValues, id]);

    return (
        <DashboardLayout title="Edytuj książkę">
            <MDBContainer>
                <BookForm defaultValues={defaultValues}/>
            </MDBContainer>
        </DashboardLayout>
    )
}

export default EditBook;

export async function getServerSideProps(context: NextPageContext) {
    return SSRAuthRedirect(context);
}
