import type { NextPage } from 'next'
import {DashboardLayout} from "../../../components/DashboardLayout/DashboardLayout";
import { MDBContainer} from "mdbreact";
import {BookForm} from "../../../components/Forms/Book";
import {NextPageContext} from "next";
import {SSRAuthRedirect} from "../../../lib/SSRAuthRedirect";

const AddBook: NextPage = () => {
    return (
        <DashboardLayout title="Dodaj książkę">
            <MDBContainer>
                <BookForm />
            </MDBContainer>
        </DashboardLayout>
    )
}

export default AddBook;

export async function getServerSideProps(context: NextPageContext) {
    return SSRAuthRedirect(context);
}
