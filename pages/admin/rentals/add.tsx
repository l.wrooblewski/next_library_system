import type { NextPage } from 'next'
import {DashboardLayout} from "../../../components/DashboardLayout/DashboardLayout";
import { MDBContainer} from "mdbreact";
import {RentalForm} from "../../../components/Forms/Rental";
import {NextPageContext} from "next";
import {SSRAuthRedirect} from "../../../lib/SSRAuthRedirect";

const AddBook: NextPage = () => {
    return (
        <DashboardLayout title="Dodaj wypożyczenie">
            <MDBContainer>
                <RentalForm />
            </MDBContainer>
        </DashboardLayout>
    )
}

export default AddBook;

export async function getServerSideProps(context: NextPageContext) {
    return SSRAuthRedirect(context);
}
