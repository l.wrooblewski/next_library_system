import type { NextPage } from 'next'
import {DashboardLayout} from "../../../../components/DashboardLayout/DashboardLayout";
import { MDBContainer} from "mdbreact";
import {useRouter} from "next/router";
import {useEffect, useState} from "react";
import {RentalForm} from "../../../../components/Forms/Rental";
import {getRentalById} from "../../../../lib/rentals";
import {NextPageContext} from "next";
import {SSRAuthRedirect} from "../../../../lib/SSRAuthRedirect";

const EditBook: NextPage = () => {
    const router = useRouter();
    const { id } = router.query;
    const [defaultValues, setDefaultValues] = useState();

    useEffect(()=> {
        if (id && !defaultValues){
            getRentalById(Number(id)).then(response => {
                setDefaultValues(response.data);
            })
        }
    }, [defaultValues, id]);

    return (
        <DashboardLayout title="Edytuj wypożyczenie">
            <MDBContainer>
                <RentalForm defaultValues={defaultValues}/>
            </MDBContainer>
        </DashboardLayout>
    )
}

export default EditBook;

export async function getServerSideProps(context: NextPageContext) {
    return SSRAuthRedirect(context);
}
