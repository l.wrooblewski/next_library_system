import type { NextPage } from 'next'
import {DashboardLayout} from "../../../components/DashboardLayout/DashboardLayout";
import {MDBAlert, MDBContainer, MDBDataTable, MDBRow} from "mdbreact";
import { MDBBtn } from "mdbreact";
import {useEffect, useState} from "react";
import {getAllRentals, IRental} from "../../../lib/rentals";
import {NextRouter, useRouter} from "next/router";
import dayjs from "dayjs";
import {getRentalStatus} from "../../../lib/helpers";
import {NextPageContext} from "next";
import {SSRAuthRedirect} from "../../../lib/SSRAuthRedirect";


const adaptData = (rentals: IRental[], router: NextRouter) => ({
    columns: [
        {
            label: 'id',
            field: 'id',
            sort: 'asc',
            width: 150
        },
        {
            label: 'Czytelnik',
            field: 'userField',
            sort: 'asc',
            width: 270
        },
        {
            label: 'Książka',
            field: 'bookField',
            sort: 'asc',
            width: 200
        },
        {
            label: 'Status',
            field: 'status',
            sort: 'asc',
            width: 200
        },
    ],
    rows: rentals.map(({id, book,dueDate,returnedDate,user }) => {
        console.log({user,book})
     return ({
         id,
         bookField: typeof(book) !== 'number' ? book?.title : book,
         userField: typeof(user) !== 'number' ? `${user?.name} ${user?.surname}` : book,
         status: getRentalStatus(dueDate, returnedDate),
         clickEvent: () => router.push('/admin/rentals/edit/'+id)
     })
    }),
});

const Index: NextPage = () => {
    const [rentals, setRentals] = useState([]);
    const router = useRouter();

    useEffect(() => {
        if (rentals.length === 0) {
            getAllRentals().then((response:any) => {
                setRentals(response.data)
            });
        }
    }, [rentals.length]);


    return (
        <DashboardLayout title="Wypożyczenia">
            <MDBContainer>
                <MDBRow start>
                    <MDBBtn className="col-3 mb-3" color="primary" onClick={() => router.push('/admin/rentals/add')}>Dodaj wypożyczenie</MDBBtn>
                </MDBRow>
            <MDBDataTable
            striped
            bordered
            hover
            infoLabel={['Wyświetlono','do','z','wpisów']}
            paginationLabel={['Wstecz', 'Dalej']}
            searchLabel="Wyszukaj"
            data={adaptData(rentals, router)}
            displayEntries={false}
        /><MDBRow start>
                <MDBAlert className="mt-4" color="info" >
                    Kliknij w rekord, aby przejść do edycji
                </MDBAlert>
            </MDBRow></MDBContainer></DashboardLayout>
    )
}

export default Index;

export async function getServerSideProps(context: NextPageContext) {
    return SSRAuthRedirect(context);
}
