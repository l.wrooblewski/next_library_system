import type { NextPage } from 'next'
import {DashboardLayout} from "../../../components/DashboardLayout/DashboardLayout";
import {MDBAlert, MDBContainer, MDBDataTable, MDBRow} from "mdbreact";
import { MDBBtn } from "mdbreact";
import {useEffect, useState} from "react";
import {getAllAuthors} from "../../../lib/authors";
import {Author} from "../../../database/models";
import {NextRouter, useRouter} from "next/router";
import {NextPageContext} from "next";
import {SSRAuthRedirect} from "../../../lib/SSRAuthRedirect";


const adaptData = (authors: Author[], router: NextRouter) => ({
    columns: [
        {
            label: 'id',
            field: 'id',
            sort: 'asc',
            width: 150
        },
        {
            label: 'Name',
            field: 'name',
            sort: 'asc',
            width: 270
        },
        {
            label: 'Surname',
            field: 'surname',
            sort: 'asc',
            width: 200
        },
    ],
    rows: authors.map(({id, name, surname}) =>
        ({
            id,
            name,
            surname,
            clickEvent: () => router.push(`/admin/authors/edit/${id}`)
        })),
});

const Authors: NextPage = () => {
    const router = useRouter();
    const [authors, setAuthors] = useState([]);

    useEffect(() => {
        if (authors.length === 0) {
            getAllAuthors().then((response:any) => setAuthors(response.data));
        }
    }, [authors.length]);


    return (
        <DashboardLayout title="Autorzy">
            <MDBContainer>
                <MDBRow start>
                    <MDBBtn className="col-3 mb-3" color="primary" onClick={() => router.push('/admin/authors/add')}>Dodaj autora</MDBBtn>
                </MDBRow>
                <MDBDataTable
                    striped
                    bordered
                    hover
                    infoLabel={['Wyświetlono','do','z','wpisów']}
                    paginationLabel={['Wstecz', 'Dalej']}
                    searchLabel="Wyszukaj"
                    data={adaptData(authors, router)}
                    displayEntries={false}
                /><MDBRow start>
                <MDBAlert className="mt-4" color="info" >
                    Kliknij w rekord, aby przejść do edycji
                </MDBAlert>
            </MDBRow></MDBContainer></DashboardLayout>
    )
}

export default Authors;

export async function getServerSideProps(context: NextPageContext) {
    return SSRAuthRedirect(context);
}
