import cookie from "cookie";
import { NextPageContext } from "next";

export const SSRAuthRedirect = (context: NextPageContext) => {
  const cookies = cookie.parse(context?.req?.headers.cookie || "");
  if (!cookies.userId || !cookies.authToken || !cookies.role) {
    context.res?.writeHead(307, { Location: `/` });
    context.res?.end();
  }
  return {
    props: {}
  };
};
