import axios from "axios";

interface Credentials {
  email: string;
  password: string;
}

export const loginUser = ({ email, password }: Credentials): any => {
  return axios
    .get("/api/user/getToken", {
      params: {
        email,
        password
      }
    })
    .then(function(response) {
      return response.data;
    })
    .catch(function(error) {
      return error;
    });
};
