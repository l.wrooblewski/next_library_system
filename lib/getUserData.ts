import axios from "axios";

export const getUserRole = (userId: number): any => {
  return axios
    .get(`/api/user/role?id=${userId}`)
    .then(function(response) {
      return response;
    })
    .catch(function(error) {
      return error;
    });
};

export const getUserData = (token: string): any => {
  return axios
    .get("/api/user", {
      params: {
        token
      }
    })
    .then(function(response) {
      return response.data;
    })
    .catch(function(error) {
      return error;
    });
};

export const getAllReaders = (): any => {
  return axios
    .get("/api/user/readers")
    .then(function(response) {
      return response.data;
    })
    .catch(function(error) {
      return error;
    });
};
