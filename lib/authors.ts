import axios from "axios";

export interface IAuthor {
  id: number;
  name: string;
  surname: string;
}

export interface AllAuthorsResponse {
  status: string;
  data: IAuthor[];
}

export const getAllAuthors = (): Promise<AllAuthorsResponse> => {
  return axios
    .get("/api/authors", {})
    .then(function(response) {
      return response.data;
    })
    .catch(function(error) {
      return error;
    });
};

export const addAuthor = ({ name, surname }: IAuthor) => {
  return axios
    .put("/api/authors", {
      name,
      surname
    })
    .then(function(response) {
      console.log(response.data);
      return response.data;
    })
    .catch(function(error) {
      return error;
    });
};

export const getAuthorById = (id: number) => {
  // @ts-ignore
  return axios
    .get(`/api/authors?id=${id}`)
    .then(function(response) {
      console.log(response.data);
      return response.data;
    })
    .catch(function(error) {
      return error;
    });
};

// @ts-ignore
export const editAuthor = (id, values) => {
  return axios
    .post(`/api/authors?id=${id}`, {
      ...values
    })
    .then(function(response) {
      console.log(response.data);
      return response.data;
    })
    .catch(function(error) {
      return error;
    });
};
