import axios from "axios";

export interface IRental {
  dueDate: string;
  returnedDate: string;
  user: number | Record<string, string>;
  book: number | Record<string, string>;
  id?: number;
  bookId?: number;
  readerId?: number;
}

export interface AllRentalsResponse {
  status: string;
  data: IRental[];
}

export const getAllReadersRentals = (
  id: number
): Promise<AllRentalsResponse> => {
  return axios
    .get(`/api/rentals?userId=${id}`, {})
    .then(function(response) {
      return response.data;
    })
    .catch(function(error) {
      return error;
    });
};

export const getAllRentals = (): Promise<AllRentalsResponse> => {
  return axios
    .get("/api/rentals", {})
    .then(function(response) {
      return response.data;
    })
    .catch(function(error) {
      return error;
    });
};

export const addRental = ({
  dueDate,
  returnedDate,
  bookId,
  readerId
}: IRental) => {
  return axios
    .put("/api/rentals", {
      dueDate,
      returnedDate,
      book: bookId,
      user: readerId
    })
    .then(function(response) {
      console.log(response.data);
      return response.data;
    })
    .catch(function(error) {
      return error;
    });
};

export const getRentalById = (id: number) => {
  // @ts-ignore
  return axios
    .get(`/api/rentals?id=${id}`)
    .then(function(response) {
      return response.data;
    })
    .catch(function(error) {
      return error;
    });
};

// @ts-ignore
export const editRental = (id, { dueDate, returnedDate, bookId, readerId }) => {
  return axios
    .post(`/api/rentals?id=${id}`, {
      dueDate,
      returnedDate,
      book: bookId,
      user: readerId,
      id
    })
    .then(function(response) {
      return response.data;
    })
    .catch(function(error) {
      return error;
    });
};
