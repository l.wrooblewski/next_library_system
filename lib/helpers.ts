import dayjs from "dayjs";

export const getRentalStatus = (dueDate: string, returnedDate: string) => {
  if (dayjs().isAfter(dueDate)) {
    return "Opóźnienie";
  }
  if (dayjs(returnedDate).isAfter(dueDate)) {
    return "Oddane z opóźnieniem";
  }
  if (dayjs(returnedDate).isBefore(dueDate)) {
    return "Oddane w terminie";
  }
  return "W wypożyczeniu";
};
