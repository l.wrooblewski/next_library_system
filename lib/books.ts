import axios from "axios";
import { IAuthor } from "./authors";

export interface IBook {
  title: string;
  isbn: string;
  publishYear: number;
  author?: IAuthor;
  authorId?: number;
}

export interface AllBooksResponse {
  status: string;
  data: IBook[];
}

export const getAllBooks = (): Promise<AllBooksResponse> => {
  return axios
    .get("/api/books", {})
    .then(function(response) {
      return response.data;
    })
    .catch(function(error) {
      return error;
    });
};

export const addBook = ({ title, isbn, publishYear, authorId }: IBook) => {
  return axios
    .put("/api/books", {
      title,
      isbn,
      publishYear,
      authorId
    })
    .then(function(response) {
      return response.data;
    })
    .catch(function(error) {
      return error;
    });
};

export const getBookById = (id: number) => {
  return axios
    .get(`/api/books?id=${id}`)
    .then(function(response) {
      return response.data;
    })
    .catch(function(error) {
      return error;
    });
};

export const editBook = (id: number, values: IBook) => {
  return axios
    .post(`/api/books?id=${id}`, {
      ...values
    })
    .then(function(response) {
      return response.data;
    })
    .catch(function(error) {
      return error;
    });
};
