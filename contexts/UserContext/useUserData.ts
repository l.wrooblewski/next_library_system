import { useContext } from "react";
import { UserContext } from "./UserContext";

export const useUserData = () => useContext(UserContext);
