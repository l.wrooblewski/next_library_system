import { createContext, ReactNode, useState } from "react";
import Cookies from "js-cookie";
import sha1 from "sha1";
import { getUserData } from "../../lib/getUserData";

export const UserContext = createContext({});

interface Props {
  children: ReactNode;
}

export interface User {
  birthday: string;
  email: string;
  gender: number;
  name: string;
  surname: string;
  id: number;
}

export const UserContextProvider = ({ children }: Props) => {
  const [userData, setUserData] = useState({});
  const userIdFromCookies = Cookies.get("userId") || "-1";
  const authToken = Cookies.get("authToken") || "null";

  if (sha1(userIdFromCookies) === authToken) {
    if (!Object.keys(userData).length) {
      getUserData(authToken).then((response: any) => {
        const { data } = response;
        setUserData(data);
      });
    }
  }

  return (
    <UserContext.Provider value={{ userData, setUserData }}>
      {children}
    </UserContext.Provider>
  );
};
